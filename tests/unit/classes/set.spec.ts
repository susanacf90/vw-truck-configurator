import { expect } from 'chai'
import Set from '@/classes/Set'

describe('Set.ts', () => {
  it('set constructor creates an object', () => {
    const newSet = new Set<string>()
    expect(newSet).to.be.an('object')
  })

  it('set constructor initializes empty set', () => {
    const newSet = new Set<string>()
    expect(newSet.size()).to.equal(0)
  })

  it('can add value to set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    expect(newSet.size()).to.equal(1)
  })

  it('can remove value from set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    expect(newSet.size()).to.equal(1)
    newSet.remove('a')
    expect(newSet.size()).to.equal(0)
  })

  it('can add different values to set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    newSet.add('b')
    expect(newSet.size()).to.equal(2)
  })

  it('cannot add equal values to set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    newSet.add('a')
    expect(newSet.size()).to.equal(1)
  })

  it('can detect whether value is present', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    expect(newSet.contains('a')).to.be.true
  })

  it('array created from set is not null', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    expect(newSet.toArray()).is.not.null
  })

  it('can create an array from set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    expect(newSet.toArray()).to.be.an('array')
  })

  it('array created from empty set is empty', () => {
    const newSet = new Set<string>()
    expect(newSet.toArray()).to.be.empty
  })

  it('array created from set has given values', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    newSet.add('b')
    expect(newSet.toArray()).to.contain('a')
    expect(newSet.toArray()).to.contain('b')
  })

  it('can remove all values from set', () => {
    const newSet = new Set<string>()
    newSet.add('a')
    newSet.add('b')
    newSet.add('c')
    expect(newSet.size()).to.equal(3)
    newSet.removeAll()
    expect(newSet.size()).to.equal(0)
  })
})
