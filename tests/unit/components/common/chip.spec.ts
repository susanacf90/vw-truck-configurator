import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Chip from '@/components/Common/Chip.vue'

const factory = (propsData?: {}) => {
  return shallowMount(Chip, {
    propsData: {
      itemId: 'chip_item_id',
      label: 'chip_label',
      ...propsData
    }
  })
}

describe('Chip.vue', () => {
  it('chip span text has the same value as chip label property', () => {
    const wrapper = factory()

    expect(wrapper.find('span').text()).equals('chip_label')
  })

  it('chip click event emitts deleted', () => {
    const wrapper = factory()

    wrapper.find('img').trigger('click')

    expect(wrapper.emitted().deleted).to.have.length(1)
  })

  it('chip click event emitts deleted with passed itemId', () => {
    const wrapper = factory()

    wrapper.find('img').trigger('click')

    expect(wrapper.emitted().deleted[0][0]).to.equal('chip_item_id')
  })
})
