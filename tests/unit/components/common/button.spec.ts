import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Button from '@/components/Common/Button.vue'

const factory = (propsData?: {}) => {
  return shallowMount(Button, {
    propsData: {
      label: 'button_label',
      ...propsData
    }
  })
}

describe('Button.vue', () => {
  it('button is disabled when disabled property is true', () => {
    const wrapper = factory({ disabled: true })

    expect(wrapper.find('button').attributes().disabled).to.exist
  })

  it('button is enabled when disabled property is false', () => {
    const wrapper = factory({ disabled: false })

    expect(wrapper.find('button').attributes().disabled).to.not.exist
  })

  it('button span text has the same value as button label property', () => {
    const wrapper = factory()

    expect(wrapper.find('button').find('span').text()).equals('button_label')
  })

  it('button click event emitts clicked', () => {
    const wrapper = factory()

    wrapper.find('button').trigger('click')

    expect(wrapper.emitted().clicked).to.have.length(1)
  })
})
