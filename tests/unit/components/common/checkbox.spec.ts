import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import Checkbox from '@/components/Common/Checkbox.vue'

const factory = (propsData?: {}) => {
  return shallowMount(Checkbox, {
    propsData: {
      identifier: 'checkbox_identifier',
      label: 'checkbox_label',
      ...propsData
    }
  })
}

describe('Checkbox.vue', () => {
  it('checkbox is not checked when checked property is false', () => {
    const wrapper = factory({ checked: false })

    expect(wrapper.find('input[type=checkbox]:checked').element).to.not.exist
  })

  it('checkbox is checked when checked property is true', () => {
    const wrapper = factory({ checked: true })

    expect(wrapper.find('input[type=checkbox]:checked').element).to.exist
  })

  it('checkbox label text has the same value as checkbox label property', () => {
    const wrapper = factory({ checked: false })

    expect(wrapper.find({ ref: 'checkbox-label' }).text()).equals('checkbox_label')
  })

  it('checkbox change event emitts changed', () => {
    const wrapper = factory({ checked: false })

    wrapper.find('input').trigger('change')

    expect(wrapper.emitted().changed).to.have.length(1)
  })
})
