import Vue from 'vue'
import VueRouter from 'vue-router'
import * as routes from '@/router/routes'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: Object.values(routes)
})

export default router
