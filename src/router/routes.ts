import { RouteConfig } from 'vue-router'
import Home from '@/views/Home.vue'

export const homeRoute: RouteConfig = {
  path: '/',
  name: 'Home',
  component: Home
}

export const combinationsRoute: RouteConfig = {
  path: '/combinations',
  name: 'Combinations',
  props: true,
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route which is lazy-loaded when the route is visited.
  component: () => import('@/views/Combinations.vue'),
  beforeEnter: (to, from, next) => {
    // if not coming from homepage (elements page), redirect to app root
    if (from.name === homeRoute.name) next()
    else next(homeRoute.path)
  }
}
