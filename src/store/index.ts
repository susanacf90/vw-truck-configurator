import Vue from 'vue'
import Vuex from 'vuex'
import Element from '@/interfaces/Element'
import ElementsResult from '@/interfaces/ElementsResult'
import Set from '@/classes/Set'
import CheckedItem from '@/interfaces/CheckedItem'
import Combination from '@/interfaces/Combination'
import ConfiguratorStepType from '@/interfaces/ConfiguratorStepType'
import ModalList from '@/interfaces/ModalList'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search: {
      searchWords: '',
      elementsResult: {
        internalElements: new Array<Element>(),
        externalElements: new Array<Element>()
      }
    },
    checkedItems: new Set<CheckedItem>(),
    combinations: new Array<Combination>(),
    configurationStep: ConfiguratorStepType.Elements,
    modal: {
      id: ModalList.Null,
      promise: { then: () => {}, catch: () => {} }
    }
  },
  mutations: {
    changeSearch (state, { searchWords, elementsResult }: { searchWords: string; elementsResult: ElementsResult }) {
      state.search.searchWords = searchWords
      state.search.elementsResult = elementsResult
    },

    addCheckedItem (state, item: CheckedItem) {
      state.checkedItems.add(item)
    },

    removeCheckedItem (state, item: CheckedItem) {
      state.checkedItems.remove(item)
    },

    removeAllCheckedItems (state) {
      state.checkedItems.removeAll()
    },

    createCombinations (state, combinations: Array<Combination>) {
      state.combinations = combinations
    },

    changeConfigurationStep (state, configurationStep: ConfiguratorStepType) {
      state.configurationStep = configurationStep
    },

    changeModal (state, { id, promise }: { id: ModalList; promise: { then: () => void; catch: () => void } }) {
      state.modal.id = id
      state.modal.promise = promise
    },

    cleanModal (state) {
      state.modal.id = ModalList.Null
      state.modal.promise = { then: () => {}, catch: () => {} }
    }
  },
  actions: {
  },
  modules: {
  }
})
