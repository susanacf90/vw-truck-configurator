export default interface Combination {
    internal: string;
    external: string;
}
