enum ElementType {
    Internal = 'internalElement',
    External = 'externalElement'
}

export default ElementType
