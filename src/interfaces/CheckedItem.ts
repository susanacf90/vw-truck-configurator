import ElementType from '@/interfaces/ElementType'

export default interface CheckedItem {
    name: string;
    type: ElementType;
}
