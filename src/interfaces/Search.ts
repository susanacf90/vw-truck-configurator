import ElementsResult from './ElementsResult';

export default interface Search {
    searchWords: string,
    elementsResult: ElementsResult
}
