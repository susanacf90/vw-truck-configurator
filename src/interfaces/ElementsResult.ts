import Element from '@/interfaces/Element'

export default interface ElementsResult {
    internalElements: Element[];
    externalElements: Element[];
}
