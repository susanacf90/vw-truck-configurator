import ModalList from '@/interfaces/ModalList'

export default interface ModalItem {
    id: ModalList;
    promise: { then: () => void; catch: () => void };
}
