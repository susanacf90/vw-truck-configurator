enum ConfiguratorStepType {
    Elements = 'elementsStep',
    Combinations = 'combinationsStep'
}

export default ConfiguratorStepType
