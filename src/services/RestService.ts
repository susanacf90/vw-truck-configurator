import axios from '@/services/Axios'
import ElementsResult from '@/interfaces/ElementsResult'

const errorHandler = (error: Error) => {
  if (!axios.isCancel(error)) console.warn(error)
}

export default class RestService {
  protocol: string = (process.env.VUE_APP_USE_HTTPS === 'true' ? 'https://' : 'http://');
  host: string = process.env.VUE_APP_HOST;

  getInternalElements (searchWords: string) {
    return axios
      .get(`${this.protocol}${this.host}/api/truck-elements/internal/$searchWords`, { params: { $searchWords: searchWords } })
      .then(response => response.data)
      .catch(errorHandler)
  };

  getExternalElements (searchWords: string) {
    return axios
      .get(`${this.protocol}${this.host}/api/truck-elements/external/$searchWords`, { params: { $searchWords: searchWords } })
      .then(response => response.data)
      .catch(errorHandler)
  };

  getPossibleCombinations (checkedItems: ElementsResult) {
    return axios
      .post(`${this.protocol}${this.host}/api/truck-elements/possible-combinations`, checkedItems)
      .then(response => response.data)
      .catch(error => {
        console.warn(error)
      })
  };
}
