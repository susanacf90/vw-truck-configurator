import axios from 'axios'

axios.interceptors.request.use((config) => {
  if (config.params) {
    for (const key in config.params) {
      if (config.params[key] === '') {
        throw new axios.Cancel()
      }

      config.url = config.url?.replace(key, config.params[key])
    }

    delete config.params
  }

  return config
})

export default axios
