export default class Set<T> {
  private setData: { [key: string]: T };

  public constructor () {
    this.setData = {}
  }

  public size (): number {
    return Object.keys(this.setData).length
  }

  public add (entry: T) {
    const newData: { [key: string]: T } = {}
    newData[`${this.hash(entry)}`] = entry
    this.setData = Object.assign({}, this.setData, newData)
  }

  public remove (entry: T) {
    const newData = Object.assign({}, this.setData)
    delete newData[`${this.hash(entry)}`]
    this.setData = Object.assign({}, newData)
  }

  public removeAll () {
    this.setData = {}
  }

  public contains (entry: T) {
    return (this.setData[`${this.hash(entry)}`] && true)
  }

  private hash (data: T): string {
    return JSON.stringify(data)
  }

  public toArray () {
    return Object.values(this.setData)
  }
}
