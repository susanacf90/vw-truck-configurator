# vw_truck_configurator

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Decisions Explanation

## Class based vs Object Oriented
I decided to do mostly class based because I prefer it to the object oriented choice, mostly for readability reasons and also because I prefer to use vuex-class (just out of
habit). The biggest downside to my choice is the fact that most of the documentation available online focuses on object oriented development. The Bundles JS is also a little
bigger on class based Vue development than on object oriented, but I think the readability compensates for it.


## State management
As mentioned in the previous paragraph, I have decided to use Vuex because since I had data I had to share between components and this data would need to travel more than
just one "level", the passing of this data as well as the state maintenance of the application state would be easier with the mentioned library. I could have used a global
object instead but since Vuex stores are reactive and the data I am passing will cause multiple components to "adapt", it is the better option, in my opinion. In addition to
this, a global object can be changed directly, whereas a Vuex store state can only be changed through mutation commit, which allows us to track the changes commited to it.


## Routes
Noticing the way I have created the routes on the application, they're generally not created the way I have done it. Typically, you create an object with all the existing
routes in it and whenever you want to call a router.push with one, for example, you will have to manually write the name or the path on the push, which may lead to some
mistyping and problems. To avoid this, I have created a variable for each route which contains its name and path and passed them on the object, and I can access the properties
instead of writing their values when I use them.


## Axios
I have used Axios to do the API calls and I have created an interceptor on it so that whenever an empty search is done, the getElements request will not be sent to the API,
because we already know that the response will also be empty. That being said, I decided to cancel the request and to render the empty "result".


## CSS
Simply for organization's sake, ease of finding whatever class we are looking for (as well as gathering them all in the same place) and being able to reuse styles that are the
same in different components I have also decided to move all CSS to a .scss file instead of building the CSS directly on each component. On each component, I am importing the
meaningful CSS files in scoped mode, so that there is no style bleeding to other components.


## Decorators
I have also used private and readonly's on certain props because, as it is, the props don't need to be changed. If this changes for some reason, the change to allow it is
simple and quick. The private decorator doesn't allow us to see that a given component has a given property, but if we know it exists we can access it just the same. The only
real advantage of private is that when the Typescript compiler tries to transpile the code to JS and finds the private decorator on a given property, it will throw an error
and not let us keep compiling.


## General
On ButtonManager component, I could have dynamically defined the parameters, calling a function for each one. Simply for readability and out of preference, I handled the
parameter values directly on the HTML.
I created a Set structure instead of using the Vue one. I have decided to do this because the Vue Set cannot compare custom objects (like the chackedItems on the ResultTable
component) out of the box.
The modal could have been created as a generic component if I passed its content from the place that uses it to the modal itself, instead of declaring the content directly
inside the modal (which I have done because we've just really got the one modal). Despite this, the close and resolve actions are generic.
In a similar way, the footer could call a footer-content component so that it too would be a completely generic, customizable component dependent on, for example, the
configuration step (Vuex state) we are on. Because on the mockups the footer always looks the same (except for the button) and also because I have built this logic on the
ButtonManager component, I thought that would be going a little too far with the component creation. If there was different footer content on the different screens of the
mockup, I would definitely opt for this approach.


## Tests
I couldn't do all the tests I wanted and I didn't do every test I could possible do. I opted for not building some "smaller" tests, like the existence of x, y or z prop or if
the prop values are correctly passed to the component, for example. These cases are already being tested "indirectly" on "bigger" tests, for example, if in the button
component the disabled prop doesn't exist and its existence test fails, the 'button is disabled when disabled property is true' test will also fail. Not building these simpler
tests allows us to test the same cases writing much less code and make changes to the existing components without compromising a big number of its tests, and all we will have
to do is add new tests to test the changes and/or change the few ones that actually broke and fail now. As an example, if we have a color prop on the button component that
is not needed anymore all of a sudden, removing the color prop will not break the existence tests on it because they don't exist to begin with. Any tests that somehow use the
now deleted color prop will break and fail and although we will have to fix them, there will be less tests to fix than if we tested for every single aspect.


# HOW TO RUN:
- docker-compose build
- docker-compose up -d
- Go to the set address (in this case, localhost:8081)
