# build stage
FROM node:lts-alpine as build-stage
ENV VUE_APP_USE_HTTPS false
ENV VUE_APP_HOST localhost:8081
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]